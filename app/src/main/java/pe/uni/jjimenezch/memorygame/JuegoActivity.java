package pe.uni.jjimenezch.memorygame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class JuegoActivity extends AppCompatActivity {

    // variables componentes de la vista
    ImageButton imageButton_1;
    ImageButton imageButton_2;
    ImageButton imageButton_3;
    ImageButton imageButton_4;
    ImageButton imageButton_5;
    ImageButton imageButton_6;
    ImageButton imageButton_7;
    ImageButton imageButton_8;
    ImageButton imageButton_9;
    ImageButton imageButton_10;
    ImageButton imageButton_11;
    ImageButton imageButton_12;
    ImageButton imageButton_13;
    ImageButton imageButton_14;
    ImageButton imageButton_15;
    ImageButton imageButton_16;

    ImageButton[] tablero = new ImageButton[16];

    Button boton_reiniciar;
    Button boton_salir;

    TextView textView_puntuacion;

    int puntuacion;
    int aciertos;

    //Imagenes

    int[] imagenes;
    int fondo;

    //Variables del juego

    ArrayList<Integer> arrayDesordenado;
    //es necesario para comparar entre el que tenemos y el que viene
    ImageButton primero;
    int numeroPrimero, numeroSegundo;

    //en el momento que hayan 2 cartas destapadas tudo se bloquea
    boolean bloqueo = false;

    // un temporizador

    final Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);
        init();


    }

    private void cargarTablero(){

        imageButton_1 = findViewById(R.id.imageButton_1);
        imageButton_2 = findViewById(R.id.imageButton_2);
        imageButton_3 = findViewById(R.id.imageButton_3);
        imageButton_4 = findViewById(R.id.imageButton_4);
        imageButton_5 = findViewById(R.id.imageButton_5);
        imageButton_6 = findViewById(R.id.imageButton_6);
        imageButton_7 = findViewById(R.id.imageButton_7);
        imageButton_8 = findViewById(R.id.imageButton_8);
        imageButton_9 = findViewById(R.id.imageButton_9);
        imageButton_10 = findViewById(R.id.imageButton_10);
        imageButton_11 = findViewById(R.id.imageButton_11);
        imageButton_12 = findViewById(R.id.imageButton_12);
        imageButton_13 = findViewById(R.id.imageButton_13);
        imageButton_14 = findViewById(R.id.imageButton_14);
        imageButton_15 = findViewById(R.id.imageButton_15);
        imageButton_16 = findViewById(R.id.imageButton_16);

        tablero[0] = imageButton_1;
        tablero[1] = imageButton_2;
        tablero[2] = imageButton_3;
        tablero[3] = imageButton_4;
        tablero[4] = imageButton_5;
        tablero[5] = imageButton_6;
        tablero[6] = imageButton_7;
        tablero[7] = imageButton_8;
        tablero[8] = imageButton_9;
        tablero[9] = imageButton_10;
        tablero[10] = imageButton_11;
        tablero[11] = imageButton_12;
        tablero[12] = imageButton_13;
        tablero[13] = imageButton_14;
        tablero[14] = imageButton_15;
        tablero[15] = imageButton_16;


    }

    private void cargarBotones(){
        boton_reiniciar = findViewById(R.id.boton_reiniciar);
        boton_salir = findViewById(R.id.boton_salir);
        boton_reiniciar.setOnClickListener(view -> init());

        boton_salir.setOnClickListener(view -> finish());

    }

    private void cargarTexto(){
        textView_puntuacion = findViewById(R.id.textView_puntuacion);
        puntuacion = 0;
        aciertos = 0;

        textView_puntuacion.setText(R.string.Puntuacion + puntuacion);

    }

    private void cargarImagenes(){
        imagenes = new int[]{
                R.drawable.la0,
                R.drawable.la1,
                R.drawable.la2,
                R.drawable.la3,
                R.drawable.la4,
                R.drawable.la5,
                R.drawable.la6,
                R.drawable.la7
        };

        fondo = R.drawable.fondo;
    }


    // desordenando el tablero
    private ArrayList<Integer> barajar(int longitud){
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i=0; i<longitud*2; i++){
            result.add(i % longitud);
        }
        Collections.shuffle(result);
        System.out.println(Arrays.toString(result.toArray()));
        return result;
    }



    // private void comprobar(int i, final ImageButton imgb){
    private void comprobar(int i, final ImageButton imgb) {

        if(primero == null){
            primero = imgb;
            primero.setScaleType(ImageView.ScaleType.CENTER_CROP);
            primero.setImageResource(imagenes[arrayDesordenado.get(i)]);
            primero.setEnabled(false);
            numeroPrimero = arrayDesordenado.get(i);

        }else{
            bloqueo = true;
            imgb.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imgb.setImageResource(imagenes[arrayDesordenado.get(i)]);
            imgb.setEnabled(false);
            numeroSegundo = arrayDesordenado.get(i);
            if(numeroPrimero == numeroSegundo){
                primero = null;
                bloqueo = false;
                aciertos++;
                puntuacion++;

                Resources res =getResources();
                textView_puntuacion.setText(String.format(res.getString(R.string.Puntuacionmensaje),puntuacion));
                if(aciertos == imagenes.length){
                    Toast toast = Toast.makeText(getApplicationContext(),R.string.Felicidades, Toast.LENGTH_LONG);
                    toast.show();
                }
            }else{
                handler.postDelayed(() -> {
                    primero.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    primero.setImageResource(fondo);
                    primero.setEnabled(true);

                    imgb.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imgb.setImageResource(fondo);
                    imgb.setEnabled(true);
                    bloqueo = false ;
                    primero = null;
                    puntuacion--;
                    Resources res =getResources();
                    textView_puntuacion.setText(String.format(res.getString(R.string.Puntuacionmensaje),puntuacion));
                }, 100);

            }
        }

    }


    private void init(){
        cargarTablero();
        cargarBotones();
        cargarTexto();
        cargarImagenes();
        arrayDesordenado = barajar(imagenes.length);

        for(int i = 0; i< tablero.length; i++ ){
            tablero[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
            tablero[i].setImageResource(imagenes[arrayDesordenado.get(i)]);
            //tablero[i].setImageResource(fondo);
        }

        //permite que se puedan ver las imagenes por un intervalo de tiempo
        handler.postDelayed(() -> {
            for(int i=0; i<tablero.length; i++){
                tablero[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                //tablero[i].setImageResource(imagenes[arrayDesordenado.get(i)]);
                tablero[i].setImageResource(fondo);
            }
        }, 500);


        for(int i=0; i<tablero.length; i++) {
            final int j = i;
            tablero[i].setEnabled(true);
            tablero[i].setOnClickListener(v -> {
                if(!bloqueo)
                    comprobar(j, tablero[j]);
            });
        }
    }
}