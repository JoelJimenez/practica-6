package pe.uni.jjimenezch.memorygame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    TextView textView_splash_1;
    TextView textView_splash_2;
    ImageView imageView_splash_1;

    Animation animationImage, animationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        textView_splash_1 = findViewById(R.id.textView_splash_1);

        textView_splash_2 = findViewById(R.id.textView_splash_2);

        imageView_splash_1 = findViewById(R.id.imageView_splash_1);


        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageView_splash_1.setAnimation(animationImage);
        textView_splash_1.setAnimation(animationText);
        textView_splash_2.setAnimation(animationText);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        }.start();

    }
}