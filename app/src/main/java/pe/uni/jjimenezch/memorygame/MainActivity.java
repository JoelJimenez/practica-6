package pe.uni.jjimenezch.memorygame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button boton_1;
    Button boton_2;
    Button boton_3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton_1 = findViewById(R.id.boton_1);

        boton_2 = findViewById(R.id.boton_2);

        boton_3 = findViewById(R.id.boton_3);


        boton_1.setOnClickListener(view -> {
            Intent intent = new Intent(this,JuegoActivity.class);
            startActivity(intent);

        });
        boton_2.setOnClickListener(view -> finish());

        boton_3.setOnClickListener(view ->  {
            Intent intent2 = new Intent(this,InstruccionesActivity.class);
            startActivity(intent2);
        });
    }
}